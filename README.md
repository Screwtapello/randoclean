# Randoclean

A randomized to-do list tool.

## Getting started

Randoclean is written in [Rust][r], and uses Rust's standard packaging tool, Cargo, so if you have Rust and Cargo installed, you can build it with the usual `cargo build` command.

Randoclean requires a data-file to read and update. It should be a CSV file with the following format:

- The first line is the column headers, "Last Completed", "Interval", and "Description"
- Every following row is a new task.
- The first column contains the date on which the task was last completed, in ISO8601 format (`YYYY-MM-DD`)
- The second column is the interval in days between one iteration of the task and the next, as a decimal integer.
- The third column is a human-readable description of the task.

For example, this would be a valid input file:

    Last Completed,Interval,Description
    2016-04-01,7,Backup servers
    2016-03-29,14,Clip dog toenails
    2016-04-09,60,Shave yak

It's common for these tasks to be cleaning or maintenance related, hence the 'clean' in the name.

## Usage

Run `randoclean` and give it the path to your task file:

    randoclean path/to/input.csv

Randoclean will randomly pick a task for you, weighted by how long it's been since it was last done, compared to the interval. For example, a thing that should be done every two weeks will be a bit more likely to show up after three weeks, but a thing that should be done every day will almost certainly show up every time after one week.

Randoclean prints the task, and lets you claim it or not. You might not want to claim it if you don't have time, or you're out of cleaning supplies or whatever.

    Today, you should: Shave yak
    Will you do this [y/N]?

Every time you hit Enter or type something beginning with "n", Randoclean will randomly choose again (it may choose the same task) until you type something beginning with "y". When you do, Randoclean will update the "Last Completion" column of that task and write the task file back out to disk.

[r]: https://www.rust-lang.org/