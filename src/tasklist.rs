extern crate chrono;
extern crate rand;
extern crate csv;
use self::rand::distributions::IndependentSample;
use task::Task;

pub trait TaskList {
    fn total_weights(&self) -> f32;
    fn task_at_mut(&mut self, pointer: f32) -> Option<&mut Task>;
    fn random_task(&mut self) -> Option<&mut Task>;
}

impl TaskList for Vec<Task> {
    fn total_weights(&self) -> f32 {
        self.iter().fold(0.0, |prev, curr| prev + curr.weight())
    }

    fn task_at_mut(&mut self, pointer: f32) -> Option<&mut Task> {
        let mut total_weight = 0.0;
        for t in self.iter_mut() {
            let task_weight = t.weight();

            if pointer < total_weight + task_weight {
                return Some(t);
            }

            total_weight += task_weight;
        }

        None
    }

    fn random_task(&mut self) -> Option<&mut Task> {
        if self.len() == 0 {
            None
        } else {
            let between = rand::distributions::range::Range::new(0.0, self.total_weights());
            let mut rng = rand::thread_rng();

            // Apparently there's a chance that sampling in a float range will return a value that's
            // not actually inside the range, so this isn't as safe as it looks. Still, I'm not sure
            // how to do otherwise.
            self.task_at_mut(between.ind_sample(&mut rng))
        }
    }
}

#[test]
fn task_list_calculates_total_weight() {
    let v = vec![
        Task::new(
            chrono::Local::today().pred().pred(),
            chrono::Duration::days(1),
            "Do stuff".into(),
        ),
        Task::new(
            chrono::Local::today().pred().pred().pred(),
            chrono::Duration::days(2),
            "Do stuff".into(),
        ),
    ];

    assert_eq!(v.total_weights(), 3.5);
}

#[test]
fn task_list_can_be_indexed_by_weight() {
    let mut v = vec![
        Task::new(
            chrono::Local::today().pred().pred(),
            chrono::Duration::days(1),
            "Do some stuff".into(),
        ),
        Task::new(
            chrono::Local::today().pred().pred().pred(),
            chrono::Duration::days(2),
            "Do other stuff".into(),
        ),
    ];

    assert_eq!(v.task_at_mut(0.0).unwrap().description, "Do some stuff");
    assert_eq!(v.task_at_mut(1.9).unwrap().description, "Do some stuff");
    assert_eq!(v.task_at_mut(2.0).unwrap().description, "Do other stuff");
    assert_eq!(v.task_at_mut(2.1).unwrap().description, "Do other stuff");
    assert_eq!(v.task_at_mut(3.4).unwrap().description, "Do other stuff");
    assert_eq!(v.task_at_mut(3.5), None);
    assert_eq!(v.task_at_mut(3.6), None);
}

#[test]
fn task_list_can_pick_a_task() {
    let mut v = vec![
        Task::new(
            chrono::Local::today().pred().pred(),
            chrono::Duration::days(1),
            "Do stuff".into(),
        ),
        Task::new(
            chrono::Local::today().pred().pred().pred(),
            chrono::Duration::days(2),
            "Do stuff".into(),
        ),
    ];

    assert_eq!(v.random_task().unwrap().description, "Do stuff");
}
