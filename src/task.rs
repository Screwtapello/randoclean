extern crate chrono;

use std::fmt;
use std::error;
use self::chrono::offset::TimeZone;

#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct Task {
    last_completion: chrono::Date<chrono::Local>,
    interval: chrono::Duration,
    pub description: String,
}

#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum ParseError {
    FieldCount {
        got: usize,
        expected: usize,
    },
    FieldParseError(String),
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParseError::FieldCount { got, expected } => {
                write!(f,
                       "Got {got} fields, expected {expected}",
                       got = got,
                       expected = expected)
            }
            ParseError::FieldParseError(ref s) => write!(f, "{}", s),
        }
    }
}

impl error::Error for ParseError {
    fn description(&self) -> &str {
        match *self {
            ParseError::FieldCount { got: _, expected: _ } => "Got wrong number of fields.",
            ParseError::FieldParseError(_) => "A problem occurred interpreting a field",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            ParseError::FieldCount { got: _, expected: _ } => None,
            ParseError::FieldParseError(_) => None,
        }
    }
}


impl Task {
    pub fn new(last_completion: chrono::Date<chrono::Local>,
               interval: chrono::Duration,
               description: String)
               -> Task {
        Task {
            last_completion: last_completion,
            interval: interval,
            description: description,
        }
    }

    pub fn from_row(row: &[String]) -> Result<Task, ParseError> {
        if row.len() != 3 {
            return Err(ParseError::FieldCount {
                got: row.len(),
                expected: 3,
            });
        }

        // Get the data we actually want.
        let last_completion_text = &row[0];
        let interval_text = &row[1];
        let description = row[2].clone();

        // Parse the last_completion field
        let maybe_last_completion = last_completion_text.trim().parse();
        if let Err(problem) = maybe_last_completion {
            return Err(ParseError::FieldParseError(format!("Last completion: {}", problem).into()));
        }
        let naive_last_completion: chrono::NaiveDate = maybe_last_completion.unwrap();
        let last_completion = chrono::Local.from_local_date(&naive_last_completion).unwrap();

        // Parse the interval field.
        let maybe_interval = interval_text.trim().parse();
        if let Err(problem) = maybe_interval {
            return Err(ParseError::FieldParseError(format!("Interval: {}", problem).into()));
        }
        let interval = chrono::Duration::days(maybe_interval.unwrap());

        // The description field can stay as-is.

        // We've got a valid task!
        Ok(Task::new(last_completion, interval, description))
    }

    pub fn weight(&self) -> f32 {
        let today = chrono::Local::today();
        let elapsed = today - self.last_completion;

        (elapsed.num_seconds() as f32) / (self.interval.num_seconds() as f32)
    }

    pub fn mark_done(&mut self) {
        self.last_completion = chrono::Local::today();
    }

    pub fn to_row(&self) -> Vec<String> {
        vec![
            format!("{:?}", self.last_completion.naive_local()),
            format!("{}", self.interval.num_days()),
            self.description.clone(),
        ]
    }
}

#[test]
fn test_task_weight() {
    let t = Task::new(chrono::Local::today().pred().pred().pred(),
                      chrono::Duration::days(2),
                      "Do stuff".into());

    assert_eq!(t.weight(), 1.5);
}

#[test]
fn test_task_from_row_with_zero_fields() {
    let row = vec![];
    let res = Task::from_row(&row);

    assert_eq!(res,
               Err(ParseError::FieldCount {
                   got: 0,
                   expected: 3,
               }));
}

#[test]
fn test_task_from_row_with_valid_fields() {
    let row: Vec<String> = vec!["1993-04-24".into(), "7".into(), "Play mini-golf".into()];
    let res = Task::from_row(&row);

    assert_eq!(res,
               Ok(Task {
                   last_completion: chrono::Local.ymd(1993, 4, 24),
                   interval: chrono::Duration::days(7),
                   description: "Play mini-golf".into(),
               }))
}

#[test]
fn test_task_from_row_with_bad_date() {
    let row: Vec<String> = vec!["sasquatch".into(), "7".into(), "Play mini-golf".into()];
    let res = Task::from_row(&row);

    assert_eq!(res,
               Err(ParseError::FieldParseError("Last completion: input contains invalid \
                                                characters"
                                                   .into())));
}

#[test]
fn test_task_from_row_with_bad_interval() {
    let row: Vec<String> = vec!["1993-04-24".into(), "sasquatch".into(), "Play mini-golf".into()];
    let res = Task::from_row(&row);

    assert_eq!(res,
               Err(ParseError::FieldParseError("Interval: invalid digit found in string".into())));
}

#[test]
fn test_task_to_row() {
    let t = Task::new(chrono::Local.ymd(1993, 4, 24),
                      chrono::Duration::days(7),
                      "Play mini-golf".into());

    let expected: Vec<String> = vec!["1993-04-24".into(), "7".into(), "Play mini-golf".into()];
    let actual = t.to_row();

    assert_eq!(actual, expected);
}
