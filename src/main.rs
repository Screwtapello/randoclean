extern crate csv;
extern crate atomicwrites;

mod task;
mod tasklist;

fn read_tasklist<R: std::io::Read>(reader: csv::Reader<R>) -> Vec<task::Task> {
    reader.has_headers(true)
          .records()
          .enumerate()
          .filter_map(|(line0, maybe_fields)| {
              // Line numbers should use 1-based indexing
              let line = line0 + 1;

              // Ignore broken lines.
              if let Err(problem) = maybe_fields {
                  println!("Line {}: {}", line, problem);
                  return None;
              }
              let fields = maybe_fields.unwrap();

              let maybe_result = task::Task::from_row(&fields);

              if let Err(ref problem) = maybe_result {
                  println!("Line {}: {}", line, problem);
              }

              maybe_result.ok()
          })
          .collect()
}

fn extract_io_error<T>(err: csv::Result<T>) -> std::io::Result<T> {
    match err {
        Ok(val) => Ok(val),
        Err(csv::Error::Io(e)) => Err(e),
        Err(e) => Err(e).expect("Got a non-IO error!"),
    }
}

fn write_tasklist<W: std::io::Write>(rows: &[task::Task],
                                     writer: &mut csv::Writer<W>)
                                     -> std::io::Result<()> {
    try!(extract_io_error(writer.write(["Last Completion", "Interval", "Description"].iter())));

    for each in rows.iter().map(|t| t.to_row()) {
        try!(extract_io_error(writer.write(each.iter())))
    }

    Ok(())
}


fn prompt(msg: &str) -> String {
    use std::io::Write;

    let mut stdout = std::io::stdout();
    let stdin = std::io::stdin();

    let count = stdout.write(msg.as_bytes()).unwrap();
    assert!(count == msg.len(), "Couldn't write prompt!");
    stdout.flush().unwrap();

    let mut response = String::new();
    let count = stdin.read_line(&mut response).unwrap();
    assert!(count > 0, "Couldn't read response!");

    response
}


fn main() {
    use tasklist::TaskList;

    let args = std::env::args_os().skip(1).collect::<Vec<_>>();
    if args.len() != 1 {
        println!("Must provide a file to read.");
        std::process::exit(1);
    }

    let maybe_reader = csv::Reader::from_file(&args[0]);
    if let Err(problem) = maybe_reader {
        println!("Problem reading file: {}", problem);
        std::process::exit(1);
    }
    let reader = maybe_reader.unwrap();

    let mut list = read_tasklist(reader);

    loop {
        if let Some(mut task) = list.random_task() {
            println!("Today, you should: {}", task.description);

            let mut resp;
            loop {
                resp = prompt("Will you do this [y/N]? ");
                resp = resp.to_lowercase();
                if resp == "\n" {
                    resp = "n".into()
                }
                if resp.starts_with("y") || resp.starts_with("n") {
                    break;
                }
            }

            if resp.starts_with("y") {
                task.mark_done();
                break;
            }
        } else {
            println!("Can't find a task to do. Is {} empty?",
                     args[0].to_string_lossy());
            std::process::exit(0);
        }
    }

    let atomic = atomicwrites::AtomicFile::new(&args[0], atomicwrites::AllowOverwrite);
    atomic.write(|f| {
              let mut writer = csv::Writer::from_writer(f);
              write_tasklist(&list, &mut writer)
          })
          .unwrap();
}
